--- LOGUEO ---
CREATE PROCEDURE USP_AUTENTICAUSUARIO_LOGU
	@V_NOMUSUARIO NVARCHAR(10),
	@V_CLAVE      NVARCHAR(100)
AS
  BEGIN
      SELECT a.idusuario,
             a.nomusuario,
             a.clave,
             a.idper,
             a.correo,
             b.apepat + ' ' + b.apemat + ' ' + b.nombres descripcion
        FROM T_M_USUARIO a
       INNER JOIN T_M_PERSONAL b
          on a.idper = b.idper
       WHERE UPPER(a.NOMUSUARIO) = UPPER(@V_NOMUSUARIO)
         AND a.CLAVE = @V_CLAVE
         and a.fecfin > GETDATE()
         and a.estado = '1'
         and b.estado = '1'
END;
----
CREATE PROCEDURE USP_OBTENERUSUARIO_LOGU
	@N_IDUSUARIO INT
AS
  BEGIN
      SELECT a.idusuario,
             a.nomusuario,
             a.clave,
             a.idper,
             a.correo,
             b.apepat + ' ' + b.apemat + ' ' + b.nombres descripcion
        FROM CHASQUI.T_M_USUARIO a
       INNER JOIN CHASQUI.T_M_PERSONAL b
          on a.idper = b.idper
       WHERE a.IDUSUARIO = @N_IDUSUARIO
         and a.fecfin > GETDATE()
         and a.estado = '1'
         and b.estado = '1';
END;
-------
CREATE PROCEDURE USP_ACCESOSMENUPORUSUARIO_LOGU
	@N_IDUSUARIO INT
AS
  BEGIN
      SELECT DISTINCT (MEN.IDMENU),
                      MEN.NOMMENU,
                      ISNULL(MEN.DESIMAGEN, '-') DESIMAGEN,
                      MEN.SECUMENU
        FROM T_T_ROLMENU ACS
        JOIN T_M_MENU MEN
          ON MEN.IDMENU = ACS.IDMENU
       WHERE ACS.IDROL IN (SELECT IDROL
                             FROM T_T_ROLUSUARIO
                            WHERE IDUSUARIO = @N_IDUSUARIO)
         AND ACS.ESTADO = '1'
         AND MEN.ESTADO = '1'
       ORDER BY MEN.SECUMENU;
END;
---------
CREATE PROCEDURE USP_ACCESOSFORMPORUSUARIO_LOGU
	@N_IDUSUARIO INT
AS
  BEGIN
      SELECT DISTINCT (FORM.IDFORM),
                      FORM.NOMFORM,
                      ISNULL(FORM.DESIMAGEN, '-') DESIMAGEN,
                      ISNULL(FORM.RUTAFORM, '-'),
                      FORM.IDMENU,
                      FORM.SECUFORM
        FROM T_T_ROLFORM ACS
        JOIN T_M_FORMULARIO FORM
          ON FORM.IDFORM = ACS.IDFORM
       WHERE ACS.IDROL IN
             (SELECT IDROL FROM T_T_ROLUSUARIO WHERE IDUSUARIO = @N_IDUSUARIO)
         AND ACS.ESTADO = '1'
         AND FORM.ESTADO = '1'
       ORDER BY FORM.SECUFORM
END;
---------
CREATE PROCEDURE USP_NOTACCESOMENUPORUSUARIO_LOGU
	@N_IDUSUARIO INT
AS
  BEGIN
      SELECT DISTINCT (MEN.IDMENU)
        FROM T_T_ROLMENU ACS
        JOIN T_M_MENU MEN
          ON MEN.IDMENU = ACS.IDMENU
       WHERE ACS.IDROL NOT IN
             (SELECT IDROL FROM T_T_ROLUSUARIO WHERE IDUSUARIO = @N_IDUSUARIO)
         AND ACS.ESTADO = '1'
         AND MEN.ESTADO = '1';
END;
----
CREATE PROCEDURE USP_NOTACCESOSFORMPORUSUARIO_LOGU
	@N_IDUSUARIO INT
AS
  BEGIN
      SELECT DISTINCT (ACS.IDFORM), FRM.RUTAFORM, FRM.SECUFORM
        FROM T_T_ROLFORM ACS
       INNER JOIN T_M_FORMULARIO FRM
          ON FRM.IDFORM = ACS.IDFORM
       WHERE ACS.IDFORM NOT IN
             (SELECT DISTINCT (F.IDFORM)
                FROM T_T_ROLFORM F
               WHERE F.IDROL IN (SELECT IDROL
                                   FROM T_T_ROLUSUARIO
                                  WHERE IDUSUARIO = @N_IDUSUARIO)
                 AND F.ESTADO = '1')
         AND ACS.ESTADO = '1'
         AND FRM.ESTADO = '1'
       ORDER BY FRM.SECUFORM;
END;
------
CREATE PROCEDURE USP_VALIDAUSUARIO_LOGU
	@V_NOMUSUARIO NVARCHAR(10),
	@V_CORREO     NVARCHAR(50)
AS
  BEGIN
      SELECT CORREO + ',' + IDUSUARIO
        FROM T_M_USUARIO
       WHERE UPPER(NOMUSUARIO) = UPPER(@V_NOMUSUARIO)
         AND UPPER(CORREO) = UPPER(@V_CORREO)
END;
----
CREATE PROCEDURE USP_UPDATECLAVE_LOGU
	@N_IDUSUARIO      INT,
	@V_ANTERIORCLAVE  NVARCHAR(100),
	@V_NUEVACLAVE     NVARCHAR(100),
	@V_CONFIRMARCLAVE NVARCHAR(100)
AS
  BEGIN
    DECLARE @V_CLAVE NVARCHAR(100);
    
      SELECT @V_CLAVE = CLAVE
        FROM T_M_USUARIO
       WHERE IDUSUARIO = @N_IDUSUARIO;

      IF @V_NUEVACLAVE <> @V_CONFIRMARCLAVE
		BEGIN
			SELECT 'danger|Claves no coinciden'
		END
	  ELSE
		BEGIN
			IF @V_ANTERIORCLAVE <> @V_CLAVE
				BEGIN
					SELECT 'danger|No coincide la clave actual'
				END
			ELSE
				BEGIN
					UPDATE T_M_USUARIO
					   SET CLAVE = @V_NUEVACLAVE, USUMOD = @N_IDUSUARIO, FECMOD = GETDATE()
					 WHERE IDUSUARIO = @N_IDUSUARIO
        
					SELECT 'success|Clave actualizada satisfactoriamente'
				END
		END
END;
------
CREATE PROCEDURE USP_AUTOMATICUPDATECLAVE_LOGU
	@N_IDUSUARIO  INT,
	@V_NUEVACLAVE NVARCHAR(100)
AS
  BEGIN
    UPDATE T_M_USUARIO
       SET CLAVE = @V_NUEVACLAVE, USUMOD = @N_IDUSUARIO, FECMOD = GETDATE()
     WHERE IDUSUARIO = @N_IDUSUARIO
    
      SELECT 'success|Clave se envi� satisfactoriamente'
END;