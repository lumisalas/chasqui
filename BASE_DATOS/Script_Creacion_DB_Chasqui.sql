CREATE TABLE T_M_FORMULARIO
(
	IDFORM               INT IDENTITY(1,1) PRIMARY KEY,
	NOMFORM              NVARCHAR(50) NOT NULL ,
	RUTAFORM             NVARCHAR(50) NOT NULL ,
	DESIMAGEN            NVARCHAR(60) NULL ,
	SECUFORM             NUMERIC NOT NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	IDMENU               INT NOT NULL,
	
);

CREATE UNIQUE INDEX XPKT_M_FORMULARIO ON T_M_FORMULARIO
(IDFORM   ASC);

CREATE  INDEX XIF1T_M_FORMULARIO ON T_M_FORMULARIO
(IDMENU   ASC);

----

CREATE TABLE T_M_GRIFO
(
	IDGRIFO              INT IDENTITY(1,1) PRIMARY KEY,
	NOMGRIFO             NVARCHAR(100) NULL ,
	ESTADO               CHAR(1) NULL ,
	USUCRE               NUMERIC NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NULL ,
	FECMOD               DATE NULL 
);

CREATE UNIQUE INDEX XPKT_M_GRIFO ON T_M_GRIFO
(IDGRIFO   ASC);

----

CREATE TABLE T_M_MENU
(
	IDMENU               INT IDENTITY(1,1) PRIMARY KEY,
	NOMMENU              NVARCHAR(50) NOT NULL ,
	SECUMENU             NUMERIC NOT NULL ,
	DESIMAGEN            NVARCHAR(60) NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL 
);

CREATE UNIQUE INDEX XPKT_M_MENU ON T_M_MENU
(IDMENU   ASC);

----

CREATE TABLE T_M_OPERADOR
(
	IDOPERADOR           INT IDENTITY(1,1) PRIMARY KEY,
	NOMOPERADOR          VARCHAR(100) NOT NULL ,
	DIRECCION            VARCHAR(100) NOT NULL ,
	USUARIO              VARCHAR(10) NOT NULL ,
	CLAVE                VARCHAR(100) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	IDGRIFO              INT NOT NULL ,
	ESTADO               CHAR(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKT_M_OPERADOR ON T_M_OPERADOR
(IDOPERADOR   ASC);

CREATE  INDEX XIF1T_M_OPERADOR ON T_M_OPERADOR
(IDGRIFO   ASC);

----

CREATE TABLE T_M_PERSONAL
(
	IDPER                INT IDENTITY(1,1) PRIMARY KEY,
	APEPAT               NVARCHAR(50) NOT NULL ,
	APEMAT               NVARCHAR(50) NOT NULL ,
	NOMBRES              NVARCHAR(100) NOT NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	FECCRE               DATE NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	FECMOD               DATE NULL ,
	USUMOD               NUMERIC NULL ,
	NUMDOC               NVARCHAR(10) NOT NULL ,
	IDUNIDADNEGOCIO      INT NOT NULL 
);

CREATE UNIQUE INDEX XPKT_M_PERSONAL ON T_M_PERSONAL
(IDPER   ASC);

CREATE  INDEX XIF1T_M_PERSONAL ON T_M_PERSONAL
(IDUNIDADNEGOCIO   ASC);

----

CREATE TABLE T_M_ROL
(
	IDROL                INT IDENTITY(1,1) PRIMARY KEY,
	DESCRI               NVARCHAR(25) NOT NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL 
);

CREATE UNIQUE INDEX XPKT_M_ROL ON T_M_ROL
(IDROL   ASC);

----

CREATE TABLE T_M_UNIDADNEGOCIO
(
	IDUNIDADNEGOCIO      INT IDENTITY(1,1) PRIMARY KEY,
	DESFILIAL            NVARCHAR(50) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	ESTADO               CHAR(1) NOT NULL 
);

CREATE UNIQUE INDEX XPKT_M_UNIDADNEGOCIO ON T_M_UNIDADNEGOCIO
(IDUNIDADNEGOCIO   ASC);

----

CREATE TABLE T_M_USUARIO
(
	IDUSUARIO            INT IDENTITY(1,1) PRIMARY KEY,
	NOMUSUARIO           NVARCHAR(10) NOT NULL ,
	CLAVE                NVARCHAR(100) NOT NULL ,
	FECFIN               DATE NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	FECCRE               DATE NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECMOD               DATE NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	CORREO               NVARCHAR(50) NOT NULL ,
	IDPER                INT NOT NULL 
);

CREATE UNIQUE INDEX XPKT_M_USUARIO ON T_M_USUARIO
(IDUSUARIO   ASC);

CREATE  INDEX XIF1T_M_USUARIO ON T_M_USUARIO
(IDPER   ASC);

----

CREATE TABLE T_M_VEHICULO
(
	IDVEHICULO           INT IDENTITY(1,1) PRIMARY KEY,
	PLACA                NVARCHAR(10) NOT NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	IDPER                INT NULL ,
	RFID                 CHAR(20) NULL 
);

CREATE UNIQUE INDEX XPKT_M_VEHICULO ON T_M_VEHICULO
(IDVEHICULO   ASC);

CREATE  INDEX XIF1T_M_VEHICULO ON T_M_VEHICULO
(IDPER   ASC);

----

CREATE TABLE T_T_ASIGNACION_UNIDAD
(
	IDASIGNACIONUNIDAD   INT IDENTITY(1,1) PRIMARY KEY,
	ANIO                 CHAR(4) NOT NULL ,
	MES                  CHAR(2) NOT NULL ,
	TIPOGAL              CHAR(1) NOT NULL ,
	TOTALCOM             NUMERIC(10,2) NOT NULL ,
	NUMDOC               NVARCHAR(50) NULL ,
	FLADEVOL             CHAR(1) NOT NULL ,
	CANTDEVOL            NUMERIC(10,2) NULL ,
	FECHADEVOL           DATE NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	IDUNIDADNEGOCIO      INT NOT NULL 
);

CREATE UNIQUE INDEX XPKT_T_ASIGNACION_UNIDAD ON T_T_ASIGNACION_UNIDAD
(IDASIGNACIONUNIDAD   ASC);

CREATE  INDEX XIF1T_T_ASIGNACION_UNIDAD ON T_T_ASIGNACION_UNIDAD
(IDUNIDADNEGOCIO   ASC);

----

CREATE TABLE T_T_ASIGNACION_VEHICULO
(
	IDASIGNACIONVEHICULO INT IDENTITY(1,1) PRIMARY KEY,
	IDVEHICULO           INT NOT NULL ,
	IDASIGNACIONUNIDAD   INT NOT NULL ,
	TIPOGAL              CHAR(1) NOT NULL ,
	CANTGAL              NUMERIC(10,2) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	CANTGALCONSUMIDA     NUMERIC(10,2) NULL 
);

CREATE UNIQUE INDEX XPKT_T_ASIGNACION_VEHICULO ON T_T_ASIGNACION_VEHICULO
(IDASIGNACIONVEHICULO   ASC);

CREATE  INDEX XIF1T_T_ASIGNACION_VEHICULO ON T_T_ASIGNACION_VEHICULO
(IDVEHICULO   ASC);

CREATE  INDEX XIF2T_T_ASIGNACION_VEHICULO ON T_T_ASIGNACION_VEHICULO
(IDASIGNACIONUNIDAD   ASC);

----

CREATE TABLE T_T_DETCONSUMO
(
	IDDETCONSUMO         INT IDENTITY(1,1) PRIMARY KEY,
	CANTGAL              NUMERIC(10,2) NOT NULL ,
	FECDESPACHO          DATE NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	TIPOGAL              CHAR(1) NOT NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	IDVEHICULO           INT NOT NULL ,
	IDOPERADOR           INT NOT NULL 
);

CREATE UNIQUE INDEX XPKT_T_DETCONSUMO ON T_T_DETCONSUMO
(IDDETCONSUMO   ASC);

CREATE  INDEX XIF1T_T_DETCONSUMO ON T_T_DETCONSUMO
(IDVEHICULO   ASC);

CREATE  INDEX XIF2T_T_DETCONSUMO ON T_T_DETCONSUMO
(IDOPERADOR   ASC);

----

CREATE TABLE T_T_JUSTIFICACION
(
	IDJUSTIFICACION      INT IDENTITY(1,1) PRIMARY KEY,
	IDDETCONSUMO         INT NOT NULL ,
	DOCJUSTIFICACION     NVARCHAR(200) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	ESTADO               CHAR(1) NOT NULL ,
	TIPOGAL              CHAR(1) NOT NULL ,
	CANTGAL              NUMERIC(10,2) NOT NULL 
);

CREATE UNIQUE INDEX XPKT_T_JUSTIFICACION ON T_T_JUSTIFICACION
(IDJUSTIFICACION   ASC);

CREATE  INDEX XIF1T_T_JUSTIFICACION ON T_T_JUSTIFICACION
(IDDETCONSUMO   ASC);

----

CREATE TABLE T_T_ROLFORM
(
	IDROLFORM            INT IDENTITY(1,1) PRIMARY KEY,
	ESTADO               CHAR(1) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	IDFORM               INT NOT NULL ,
	IDROL                INT NOT NULL 
);

CREATE UNIQUE INDEX XPKT_T_ROLFORM ON T_T_ROLFORM
(IDROLFORM   ASC);

CREATE  INDEX XIF1T_T_ROLFORM ON T_T_ROLFORM
(IDFORM   ASC);

CREATE  INDEX XIF2T_T_ROLFORM ON T_T_ROLFORM
(IDROL   ASC);

----

CREATE TABLE T_T_ROLMENU
(
	IDROLMENU            INT IDENTITY(1,1) PRIMARY KEY,
	ESTADO               CHAR(1) NOT NULL ,
	USUCRE               NUMERIC NOT NULL ,
	USUMOD               NUMERIC NULL ,
	FECCRE               DATE NOT NULL ,
	FECMOD               DATE NULL ,
	IDROL                INT NOT NULL ,
	IDMENU               INT NOT NULL 
);

CREATE UNIQUE INDEX XPKT_T_ROLMENU ON T_T_ROLMENU
(IDROLMENU   ASC);

CREATE  INDEX XIF1T_T_ROLMENU ON T_T_ROLMENU
(IDROL   ASC);

CREATE  INDEX XIF2T_T_ROLMENU ON T_T_ROLMENU
(IDMENU   ASC);

----

CREATE TABLE T_T_ROLUSUARIO
(
	IDUSUARIOROL         INT IDENTITY(1,1) PRIMARY KEY,
	ESTADO               CHAR(1) NOT NULL ,
	IDROL                INT NOT NULL ,
	IDUSUARIO            INT NOT NULL
);

CREATE UNIQUE INDEX XPKT_T_ROLUSUARIO ON T_T_ROLUSUARIO
(IDUSUARIOROL   ASC);

CREATE  INDEX XIF1T_T_ROLUSUARIO ON T_T_ROLUSUARIO
(IDUSUARIO   ASC);

CREATE  INDEX XIF2T_T_ROLUSUARIO ON T_T_ROLUSUARIO
(IDROL   ASC);

----

ALTER TABLE T_M_FORMULARIO
	ADD CONSTRAINT R_6 FOREIGN KEY (IDMENU) REFERENCES T_M_MENU (IDMENU);

ALTER TABLE T_M_OPERADOR
	ADD CONSTRAINT R_32 FOREIGN KEY (IDGRIFO) REFERENCES T_M_GRIFO (IDGRIFO);

ALTER TABLE T_M_PERSONAL
	ADD CONSTRAINT R_35 FOREIGN KEY (IDUNIDADNEGOCIO) REFERENCES T_M_UNIDADNEGOCIO (IDUNIDADNEGOCIO);

ALTER TABLE T_M_USUARIO
	ADD CONSTRAINT R_27 FOREIGN KEY (IDPER) REFERENCES T_M_PERSONAL (IDPER);

ALTER TABLE T_M_VEHICULO
	ADD CONSTRAINT R_30 FOREIGN KEY (IDPER) REFERENCES T_M_PERSONAL (IDPER);

ALTER TABLE T_T_ASIGNACION_UNIDAD
	ADD CONSTRAINT R_36 FOREIGN KEY (IDUNIDADNEGOCIO) REFERENCES T_M_UNIDADNEGOCIO (IDUNIDADNEGOCIO);

ALTER TABLE T_T_ASIGNACION_VEHICULO
	ADD CONSTRAINT R_37 FOREIGN KEY (IDVEHICULO) REFERENCES T_M_VEHICULO (IDVEHICULO);

ALTER TABLE T_T_ASIGNACION_VEHICULO
	ADD CONSTRAINT R_38 FOREIGN KEY (IDASIGNACIONUNIDAD) REFERENCES T_T_ASIGNACION_UNIDAD (IDASIGNACIONUNIDAD);

ALTER TABLE T_T_DETCONSUMO
	ADD CONSTRAINT R_33 FOREIGN KEY (IDVEHICULO) REFERENCES T_M_VEHICULO (IDVEHICULO);

ALTER TABLE T_T_DETCONSUMO
	ADD CONSTRAINT R_34 FOREIGN KEY (IDOPERADOR) REFERENCES T_M_OPERADOR (IDOPERADOR);

ALTER TABLE T_T_JUSTIFICACION
	ADD CONSTRAINT R_39 FOREIGN KEY (IDDETCONSUMO) REFERENCES T_T_DETCONSUMO (IDDETCONSUMO);

ALTER TABLE T_T_ROLFORM
	ADD CONSTRAINT R_9 FOREIGN KEY (IDFORM) REFERENCES T_M_FORMULARIO (IDFORM);

ALTER TABLE T_T_ROLFORM
	ADD CONSTRAINT R_10 FOREIGN KEY (IDROL) REFERENCES T_M_ROL (IDROL);

ALTER TABLE T_T_ROLMENU
	ADD CONSTRAINT R_7 FOREIGN KEY (IDROL) REFERENCES T_M_ROL (IDROL);

ALTER TABLE T_T_ROLMENU
	ADD CONSTRAINT R_8 FOREIGN KEY (IDMENU) REFERENCES T_M_MENU (IDMENU);

ALTER TABLE T_T_ROLUSUARIO
	ADD CONSTRAINT R_28 FOREIGN KEY (IDUSUARIO) REFERENCES T_M_USUARIO (IDUSUARIO);

ALTER TABLE T_T_ROLUSUARIO
	ADD CONSTRAINT R_29 FOREIGN KEY (IDROL) REFERENCES T_M_ROL (IDROL);
